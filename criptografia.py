# -*- coding: utf-8 -*-

KEYS = {
    'a': 'w',
    'b': 'E',
    'c': 'x',
    'd': '1',
    'e': 'a',
    'f': 't',
    'g': '0',
    'h': 'C',
    'i': 'b',
    'j': '!',
    'k': 'z',
    'l': '8',
    'm': 'M',
    'n': 'I',
    'o': 'd',
    'p': '.',
    'q': 'U',
    'r': 'Y',
    's': 'i',
    't': '3',
    'u': ',',
    'v': 'J',
    'w': 'N',
    'x': 'f',
    'y': 'm',
    'z': 'W',
    'A': 'G',
    'B': 'S',
    'C': 'j',
    'D': 'n',
    'E': 's',
    'F': 'Q',
    'G': 'o',
    'H': 'e',
    'I': 'u',
    'J': 'g',
    'K': '2',
    'L': '9',
    'M': 'A',
    'N': '5',
    'O': '4',
    'P': '?',
    'Q': 'c',
    'R': 'r',
    'S': 'O',
    'T': 'P',
    'U': 'h',
    'V': '6',
    'W': 'q',
    'X': 'H',
    'Y': 'R',
    'Z': 'l',
    '0': 'k',
    '1': '7',
    '2': 'X',
    '3': 'L',
    '4': 'p',
    '5': 'v',
    '6': 'T',
    '7': 'V',
    '8': 'y',
    '9': 'K',
    '.': 'Z',
    ',': 'D',
    '?': 'F',
    '!': 'B',
    ' ': '&',
}

def cifrar(mensaje):
    words = mensaje.split(' ')
    mensaje_cifrado = []

    for word in words:
        word_cifrado = ''
        for letter in word:
            word_cifrado += KEYS[letter]

        mensaje_cifrado.append(word_cifrado)

    return ' '.join(mensaje_cifrado)

def descifrar(mensaje):
    words = mensaje.split(' ')
    mensaje_descifrado = []

    for word in words:
        word_descifrado = ''

        for letter in word:

            for key, value in KEYS.iteritems():
                if value == letter:
                    word_descifrado += key

        mensaje_descifrado.append(word_descifrado)

    return ' '.join(mensaje_descifrado)

def run():

    while True:

        command = str(raw_input('''---* ---* ---* ---* ---* ---*
            Bienvenido a criptografia. Que desea hacer?

            [c]ifrar mensaje
            [d]escifrar mensaje
            [s]alir
        '''))

        if command == 'c':
            mensaje = str(raw_input('Escribe tu mensaje: '))
            mensaje_cifrado = cifrar(mensaje)
            print(mensaje_cifrado)
        elif command == 'd':
            mensaje = str(raw_input('Escribe tu mensaje cifrado: '))
            mensaje_descifrado = descifrar(mensaje)
            print(mensaje_descifrado)
        elif command == 's':
            print('salir')
        else:
            print('Comando no encontrado!')

if __name__ == '__main__':
    print('M E N S A J E S  C I F R A D O S')
    run()
