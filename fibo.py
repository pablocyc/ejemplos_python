

def fibo(number):

    if number < 2:
        if number == 1: return 1
        return 0

    return fibo(number - 1) + fibo(number - 2)    

def run():
    number = int(input('Escribe un número: '))

    result = fibo(number)

    print('El término {} de Fibonocci, es: {}'.format(number, result))

if __name__ == '__main__':
    run()
