# -*- coding: utf-8 -*-

def foreign_exchange_calculator(ammount):
    clp_to_bol_rate = 0.0109316

    return clp_to_bol_rate * ammount

def run():
    print('C A L C U L A D O R A  D E  D I V I S A S')
    print('Convierte pesos chilenos a pesos bolivianos.')
    print('')

    ammount = float(raw_input('Ingresa la cantidad de pesos chilenos que quieres convertir: '))

    result = foreign_exchange_calculator(ammount)

    print('${} pesos chilenos son ${} pesos bolivianos'.format(ammount, result))
    print('')

if __name__ == '__main__':
    run()
