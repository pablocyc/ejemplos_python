# -*- coding: utf-8 -*-

"""
"abacabad"  c
"abacabaabacaba"  _
"abcdefghijklmnopqrstuvwxyziflskecznslkjfabe"  d
"bcccccccccyb"  y
"""

def primeraLetraNoRepetida(secuencia_letra):
    seen_letters = {}

    for idx, letter in enumerate(secuencia_letra):
        if letter not in seen_letters:
            seen_letters[letter] = (idx, 1)
        else:
            seen_letters[letter] = (seen_letters[letter][0], seen_letters[letter][1] + 1)

    final_letters = []
    for key, value in seen_letters.iteritems():
        if value[1] == 1:
            final_letters.append( (key, value[0]) )

    letras_no_repetidas = sorted(final_letters, key=lambda value: value[1])

    if letras_no_repetidas:
        return letras_no_repetidas[0][0]
    else:
        return '_'            

if __name__ == '__main__':
    secuencia_letra = str(raw_input('Escribe una secuencia de caracteres: '))

    result = primeraLetraNoRepetida(secuencia_letra)

    if result == '_':
        print('Todos los caracteres se repiten.')
    else:
        print('El primer caracter no repetido es: {}'.format(result))
